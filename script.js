let hamburgerBtn = document.getElementById("hamburger-btn");
let closeBtn = document.getElementById("close-btn");
let navItems = document.getElementById("nav-item-container");
let afterEffect = document.getElementById("after-effect");
let flag = true;
hamburgerBtn.addEventListener("click", () => {
  if (flag) {
    hamburgerBtn.classList.add("none");
    hamburgerBtn.classList.remove("hamburger-btn");
    closeBtn.classList.remove("close-btn");
    closeBtn.classList.add("block");
    navItems.classList.remove("mobile");
    flag = false;
  }
});
closeBtn.addEventListener("click", () => {
  if (!flag) {
    closeBtn.classList.add("close-btn");
    closeBtn.classList.remove("block");
    hamburgerBtn.classList.add("hamburger-btn");
    hamburgerBtn.classList.remove("none");
    navItems.classList.add("mobile");
    flag = true;
  }
});
